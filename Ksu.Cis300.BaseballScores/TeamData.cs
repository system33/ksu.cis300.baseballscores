﻿/* TeamData
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksu.Cis300.BaseballScores
{
    /// <summary>
    /// Stores data found in TEAMABR.csv
    /// about teams
    /// </summary>
    public class TeamData : IComparable<TeamData>
    {
        /// <summary>
        /// Stores the information about this team
        /// </summary>
        private string[] _data;
        /// <summary>
        /// the following properties get the 
        /// named valeus out of _data
        /// </summary>
        public string Abbreviation { get { return _data[0]; } }
        public string City         { get { return _data[1]; } }
        public string NickName     { get { return _data[2]; } }
        public string FirstYear    { get { return _data[3]; } }
        public string LastYear     { get { return _data[4]; } }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="data">array of 5 strings to store here</param>
        public TeamData(string[] data) { _data = data; }
        /// <summary>
        /// override of ToString() to print data in the form
        /// city nickname (fyear-lyear)
        /// </summary>
        /// <returns>string(city nickname (fyear-lyear))</returns>
        public override string ToString() { return City + " " + NickName + " (" + FirstYear + "-" + LastYear + ")"; }
        /// <summary>
        /// allows sorting alphabetically when data
        /// is displayed "city nickname (fyear-lyear)"
        /// </summary>
        /// <param name="other">the other TeamData instance</param>
        /// <returns>pos int if this is greater, 0 if same, neg int if this is less</returns>
        public int CompareTo(TeamData other) { return ToString().CompareTo(other.ToString()); }
    }
}
