﻿/* TeamAndDate.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksu.Cis300.BaseballScores
{
    /// <summary>
    /// Stores info for keying unique 
    /// entries in a dictionary of games
    /// </summary>
    public struct TeamAndDate
    {
        /// <summary>
        /// private fields to store this information
        /// </summary>
        private string _abbreviation;
        private DateTime _date;
        /// <summary>
        /// stores hash code for GetHashCode()
        /// calculated in ctor
        /// </summary>
        private int _hash;
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="teamAbbreviation">abbreviation for team</param>
        /// <param name="date">date for a game</param>
        public TeamAndDate(string teamAbbreviation, DateTime date)
        {
            _abbreviation = teamAbbreviation;
            _date = date;
            unchecked
            {
                const int MULT = 39;
                _hash = 0;
                for (int i = 0; i < _abbreviation.Length; i++)
                    _hash = (_hash * MULT) + _abbreviation[i];
                _hash = (_hash * MULT) + _date.Year;
                _hash = (_hash * MULT) + _date.Month;
                _hash = (_hash * MULT) + _date.Day;
            }
        }
        /// <summary>
        /// override for GetHashCode()
        /// hash was calculated in ctor, so return it
        /// </summary>
        /// <returns>hash code</returns>
        public override int GetHashCode() { return _hash; }
        /// <summary>
        /// operator to test for equality
        /// </summary>
        /// <param name="x">lhs</param>
        /// <param name="y">rhs</param>
        /// <returns>true if abbreviation and year/month/date are same, else false</returns>
        public static bool operator ==(TeamAndDate x, TeamAndDate y)
        {
            if      (x._abbreviation != y._abbreviation) return false;
            else if (x._date.Year    != y._date.Year   ) return false;
            else if (x._date.Month   != y._date.Month  ) return false;
            else if (x._date.Day     != y._date.Day    ) return false;
            else return true;
        }
        /// <summary>
        /// operator to test for inequality
        /// </summary>
        /// <param name="x">lhs</param>
        /// <param name="y">rhs</param>
        /// <returns>opposite of ==</returns>
        public static bool operator !=(TeamAndDate x, TeamAndDate y) { return !(x == y); }
        /// <summary>
        /// generate .Equals() operator
        /// </summary>
        /// <param name="obj">object to see if equal</param>
        /// <returns>true if obj is TeamAndDate and ==, otherwise false</returns>
        public override bool Equals(object obj)
        {
            if (obj is TeamAndDate) return this == (TeamAndDate)obj;
            else return false;
        }
    }
}
