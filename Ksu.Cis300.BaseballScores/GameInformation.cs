﻿/* GameInformation.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ksu.Cis300.BaseballScores
{
    public partial class GameInformation : Form
    {
        public GameInformation()
        {
            InitializeComponent();
        }
        public GameData Data
        {
            set
            {
                Text                      = value.Date.ToShortDateString();
                uxTextHome.Text           = value.HomeTeam;
                uxTextVisitor.Text        = value.AwayTeam;
                uxTextHomeScore.Text      = value.HomeTeamScore;
                uxTextVisitorScore.Text   = value.AwayTeamScore;
                uxTextWinningPitcher.Text = value.WinningPitcher;
                uxTextLosingPitcher.Text  = value.LosingPitcher;
                uxTextSave.Text           = value.SavingPitcher;
                uxTextGameWinningRBI.Text = value.WinningBatter;
            }
        }
    }
}
