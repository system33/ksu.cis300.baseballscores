﻿namespace Ksu.Cis300.BaseballScores
{
    partial class GameInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxLabelHome = new System.Windows.Forms.Label();
            this.uxLabelVisitor = new System.Windows.Forms.Label();
            this.uxTextHome = new System.Windows.Forms.TextBox();
            this.uxTextVisitor = new System.Windows.Forms.TextBox();
            this.uxLabelHomeScore = new System.Windows.Forms.Label();
            this.uxLabelVisitorScore = new System.Windows.Forms.Label();
            this.uxTextHomeScore = new System.Windows.Forms.TextBox();
            this.uxTextVisitorScore = new System.Windows.Forms.TextBox();
            this.uxLabelWinningPitcher = new System.Windows.Forms.Label();
            this.uxTextWinningPitcher = new System.Windows.Forms.TextBox();
            this.uxTextLosingPitcher = new System.Windows.Forms.TextBox();
            this.uxTextSave = new System.Windows.Forms.TextBox();
            this.uxTextGameWinningRBI = new System.Windows.Forms.TextBox();
            this.uxLabelLosingPitcher = new System.Windows.Forms.Label();
            this.uxLabelSave = new System.Windows.Forms.Label();
            this.uxLabelGameWinningRBI = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // uxLabelHome
            // 
            this.uxLabelHome.AutoSize = true;
            this.uxLabelHome.Location = new System.Drawing.Point(12, 15);
            this.uxLabelHome.Name = "uxLabelHome";
            this.uxLabelHome.Size = new System.Drawing.Size(38, 13);
            this.uxLabelHome.TabIndex = 0;
            this.uxLabelHome.Text = "Home:";
            // 
            // uxLabelVisitor
            // 
            this.uxLabelVisitor.AutoSize = true;
            this.uxLabelVisitor.Location = new System.Drawing.Point(12, 41);
            this.uxLabelVisitor.Name = "uxLabelVisitor";
            this.uxLabelVisitor.Size = new System.Drawing.Size(38, 13);
            this.uxLabelVisitor.TabIndex = 1;
            this.uxLabelVisitor.Text = "Visitor:";
            // 
            // uxTextHome
            // 
            this.uxTextHome.Enabled = false;
            this.uxTextHome.Location = new System.Drawing.Point(53, 12);
            this.uxTextHome.Name = "uxTextHome";
            this.uxTextHome.ReadOnly = true;
            this.uxTextHome.Size = new System.Drawing.Size(100, 20);
            this.uxTextHome.TabIndex = 2;
            // 
            // uxTextVisitor
            // 
            this.uxTextVisitor.Enabled = false;
            this.uxTextVisitor.Location = new System.Drawing.Point(53, 38);
            this.uxTextVisitor.Name = "uxTextVisitor";
            this.uxTextVisitor.ReadOnly = true;
            this.uxTextVisitor.Size = new System.Drawing.Size(100, 20);
            this.uxTextVisitor.TabIndex = 3;
            // 
            // uxLabelHomeScore
            // 
            this.uxLabelHomeScore.AutoSize = true;
            this.uxLabelHomeScore.Location = new System.Drawing.Point(159, 15);
            this.uxLabelHomeScore.Name = "uxLabelHomeScore";
            this.uxLabelHomeScore.Size = new System.Drawing.Size(38, 13);
            this.uxLabelHomeScore.TabIndex = 4;
            this.uxLabelHomeScore.Text = "Score:";
            // 
            // uxLabelVisitorScore
            // 
            this.uxLabelVisitorScore.AutoSize = true;
            this.uxLabelVisitorScore.Location = new System.Drawing.Point(159, 41);
            this.uxLabelVisitorScore.Name = "uxLabelVisitorScore";
            this.uxLabelVisitorScore.Size = new System.Drawing.Size(38, 13);
            this.uxLabelVisitorScore.TabIndex = 5;
            this.uxLabelVisitorScore.Text = "Score:";
            // 
            // uxTextHomeScore
            // 
            this.uxTextHomeScore.Enabled = false;
            this.uxTextHomeScore.Location = new System.Drawing.Point(203, 12);
            this.uxTextHomeScore.Name = "uxTextHomeScore";
            this.uxTextHomeScore.ReadOnly = true;
            this.uxTextHomeScore.Size = new System.Drawing.Size(33, 20);
            this.uxTextHomeScore.TabIndex = 6;
            // 
            // uxTextVisitorScore
            // 
            this.uxTextVisitorScore.Enabled = false;
            this.uxTextVisitorScore.Location = new System.Drawing.Point(203, 38);
            this.uxTextVisitorScore.Name = "uxTextVisitorScore";
            this.uxTextVisitorScore.ReadOnly = true;
            this.uxTextVisitorScore.Size = new System.Drawing.Size(33, 20);
            this.uxTextVisitorScore.TabIndex = 7;
            // 
            // uxLabelWinningPitcher
            // 
            this.uxLabelWinningPitcher.AutoSize = true;
            this.uxLabelWinningPitcher.Location = new System.Drawing.Point(12, 72);
            this.uxLabelWinningPitcher.Name = "uxLabelWinningPitcher";
            this.uxLabelWinningPitcher.Size = new System.Drawing.Size(85, 13);
            this.uxLabelWinningPitcher.TabIndex = 8;
            this.uxLabelWinningPitcher.Text = "Winning Pitcher:";
            // 
            // uxTextWinningPitcher
            // 
            this.uxTextWinningPitcher.Enabled = false;
            this.uxTextWinningPitcher.Location = new System.Drawing.Point(116, 69);
            this.uxTextWinningPitcher.Name = "uxTextWinningPitcher";
            this.uxTextWinningPitcher.ReadOnly = true;
            this.uxTextWinningPitcher.Size = new System.Drawing.Size(120, 20);
            this.uxTextWinningPitcher.TabIndex = 9;
            // 
            // uxTextLosingPitcher
            // 
            this.uxTextLosingPitcher.Enabled = false;
            this.uxTextLosingPitcher.Location = new System.Drawing.Point(116, 95);
            this.uxTextLosingPitcher.Name = "uxTextLosingPitcher";
            this.uxTextLosingPitcher.ReadOnly = true;
            this.uxTextLosingPitcher.Size = new System.Drawing.Size(120, 20);
            this.uxTextLosingPitcher.TabIndex = 10;
            // 
            // uxTextSave
            // 
            this.uxTextSave.Enabled = false;
            this.uxTextSave.Location = new System.Drawing.Point(116, 121);
            this.uxTextSave.Name = "uxTextSave";
            this.uxTextSave.ReadOnly = true;
            this.uxTextSave.Size = new System.Drawing.Size(120, 20);
            this.uxTextSave.TabIndex = 11;
            // 
            // uxTextGameWinningRBI
            // 
            this.uxTextGameWinningRBI.Enabled = false;
            this.uxTextGameWinningRBI.Location = new System.Drawing.Point(116, 147);
            this.uxTextGameWinningRBI.Name = "uxTextGameWinningRBI";
            this.uxTextGameWinningRBI.ReadOnly = true;
            this.uxTextGameWinningRBI.Size = new System.Drawing.Size(120, 20);
            this.uxTextGameWinningRBI.TabIndex = 12;
            // 
            // uxLabelLosingPitcher
            // 
            this.uxLabelLosingPitcher.AutoSize = true;
            this.uxLabelLosingPitcher.Location = new System.Drawing.Point(12, 98);
            this.uxLabelLosingPitcher.Name = "uxLabelLosingPitcher";
            this.uxLabelLosingPitcher.Size = new System.Drawing.Size(77, 13);
            this.uxLabelLosingPitcher.TabIndex = 13;
            this.uxLabelLosingPitcher.Text = "Losing Pitcher:";
            // 
            // uxLabelSave
            // 
            this.uxLabelSave.AutoSize = true;
            this.uxLabelSave.Location = new System.Drawing.Point(12, 124);
            this.uxLabelSave.Name = "uxLabelSave";
            this.uxLabelSave.Size = new System.Drawing.Size(35, 13);
            this.uxLabelSave.TabIndex = 14;
            this.uxLabelSave.Text = "Save:";
            // 
            // uxLabelGameWinningRBI
            // 
            this.uxLabelGameWinningRBI.AutoSize = true;
            this.uxLabelGameWinningRBI.Location = new System.Drawing.Point(12, 150);
            this.uxLabelGameWinningRBI.Name = "uxLabelGameWinningRBI";
            this.uxLabelGameWinningRBI.Size = new System.Drawing.Size(98, 13);
            this.uxLabelGameWinningRBI.TabIndex = 15;
            this.uxLabelGameWinningRBI.Text = "Game-Winning RBI";
            // 
            // GameInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 179);
            this.Controls.Add(this.uxLabelGameWinningRBI);
            this.Controls.Add(this.uxLabelSave);
            this.Controls.Add(this.uxLabelLosingPitcher);
            this.Controls.Add(this.uxTextGameWinningRBI);
            this.Controls.Add(this.uxTextSave);
            this.Controls.Add(this.uxTextLosingPitcher);
            this.Controls.Add(this.uxTextWinningPitcher);
            this.Controls.Add(this.uxLabelWinningPitcher);
            this.Controls.Add(this.uxTextVisitorScore);
            this.Controls.Add(this.uxTextHomeScore);
            this.Controls.Add(this.uxLabelVisitorScore);
            this.Controls.Add(this.uxLabelHomeScore);
            this.Controls.Add(this.uxTextVisitor);
            this.Controls.Add(this.uxTextHome);
            this.Controls.Add(this.uxLabelVisitor);
            this.Controls.Add(this.uxLabelHome);
            this.Name = "GameInformation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label uxLabelHome;
        private System.Windows.Forms.Label uxLabelVisitor;
        private System.Windows.Forms.TextBox uxTextHome;
        private System.Windows.Forms.TextBox uxTextVisitor;
        private System.Windows.Forms.Label uxLabelHomeScore;
        private System.Windows.Forms.Label uxLabelVisitorScore;
        private System.Windows.Forms.TextBox uxTextHomeScore;
        private System.Windows.Forms.TextBox uxTextVisitorScore;
        private System.Windows.Forms.Label uxLabelWinningPitcher;
        private System.Windows.Forms.TextBox uxTextWinningPitcher;
        private System.Windows.Forms.TextBox uxTextLosingPitcher;
        private System.Windows.Forms.TextBox uxTextSave;
        private System.Windows.Forms.TextBox uxTextGameWinningRBI;
        private System.Windows.Forms.Label uxLabelLosingPitcher;
        private System.Windows.Forms.Label uxLabelSave;
        private System.Windows.Forms.Label uxLabelGameWinningRBI;
    }
}