﻿namespace Ksu.Cis300.BaseballScores
{
    partial class UserInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxButtonAddDataFile = new System.Windows.Forms.Button();
            this.uxComboBox = new System.Windows.Forms.ComboBox();
            this.uxDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.uxButtonGetGameResults = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // uxButtonAddDataFile
            // 
            this.uxButtonAddDataFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxButtonAddDataFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxButtonAddDataFile.Location = new System.Drawing.Point(12, 12);
            this.uxButtonAddDataFile.Name = "uxButtonAddDataFile";
            this.uxButtonAddDataFile.Size = new System.Drawing.Size(301, 33);
            this.uxButtonAddDataFile.TabIndex = 0;
            this.uxButtonAddDataFile.Text = "Add Data File";
            this.uxButtonAddDataFile.UseVisualStyleBackColor = true;
            this.uxButtonAddDataFile.Click += new System.EventHandler(this.uxButtonAddDataFile_Click);
            // 
            // uxComboBox
            // 
            this.uxComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxComboBox.Enabled = false;
            this.uxComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxComboBox.FormattingEnabled = true;
            this.uxComboBox.Location = new System.Drawing.Point(12, 51);
            this.uxComboBox.Name = "uxComboBox";
            this.uxComboBox.Size = new System.Drawing.Size(301, 28);
            this.uxComboBox.Sorted = true;
            this.uxComboBox.TabIndex = 1;
            // 
            // uxDateTimePicker
            // 
            this.uxDateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.uxDateTimePicker.Location = new System.Drawing.Point(12, 86);
            this.uxDateTimePicker.MaxDate = new System.DateTime(2013, 12, 31, 0, 0, 0, 0);
            this.uxDateTimePicker.MinDate = new System.DateTime(1871, 1, 1, 0, 0, 0, 0);
            this.uxDateTimePicker.Name = "uxDateTimePicker";
            this.uxDateTimePicker.Size = new System.Drawing.Size(111, 26);
            this.uxDateTimePicker.TabIndex = 2;
            this.uxDateTimePicker.Value = new System.DateTime(2013, 12, 31, 0, 0, 0, 0);
            // 
            // uxButtonGetGameResults
            // 
            this.uxButtonGetGameResults.Enabled = false;
            this.uxButtonGetGameResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxButtonGetGameResults.Location = new System.Drawing.Point(129, 85);
            this.uxButtonGetGameResults.Name = "uxButtonGetGameResults";
            this.uxButtonGetGameResults.Size = new System.Drawing.Size(184, 33);
            this.uxButtonGetGameResults.TabIndex = 3;
            this.uxButtonGetGameResults.Text = "Get Game Results";
            this.uxButtonGetGameResults.UseVisualStyleBackColor = true;
            this.uxButtonGetGameResults.Click += new System.EventHandler(this.uxButtonGetGameResults_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // UserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 127);
            this.Controls.Add(this.uxButtonGetGameResults);
            this.Controls.Add(this.uxDateTimePicker);
            this.Controls.Add(this.uxComboBox);
            this.Controls.Add(this.uxButtonAddDataFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "UserInterface";
            this.Text = "Baseball Scores";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button uxButtonAddDataFile;
        private System.Windows.Forms.ComboBox uxComboBox;
        private System.Windows.Forms.DateTimePicker uxDateTimePicker;
        private System.Windows.Forms.Button uxButtonGetGameResults;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

