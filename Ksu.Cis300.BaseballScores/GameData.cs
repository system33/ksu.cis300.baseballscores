﻿/* GameData.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksu.Cis300.BaseballScores
{
    /// <summary>
    /// stores data about a game
    /// </summary>
    public class GameData
    {
        /// <summary>
        /// the following members store all the 
        /// valued information about this game
        /// </summary>
        private DateTime _date;
        private string _homeTeam; // ex: "Kansas City Royals" or "location name"
        private string _homeTeamAbbreviation;
        private string _homeTeamScore;
        private string _awayTeam; // same ex as above
        private string _awayTeamAbbreviation;
        private string _awayTeamScore;
        private string _winningPitcher;
        private string _losingPitcher;
        private string _savingPitcher;
        private string _winningBatter;
        /// <summary>
        /// and the following properties return
        /// the values of the above members
        /// </summary>
        public DateTime Date               { get { return _date;                 } }
        public string HomeTeam             { get { return _homeTeam;             } }
        public string HomeTeamAbbreviation { get { return _homeTeamAbbreviation; } }
        public string HomeTeamScore        { get { return _homeTeamScore;        } }
        public string AwayTeam             { get { return _awayTeam;             } }
        public string AwayTeamAbbreviation { get { return _awayTeamAbbreviation; } }
        public string AwayTeamScore        { get { return _awayTeamScore;        } }
        public string WinningPitcher       { get { return _winningPitcher;       } }
        public string LosingPitcher        { get { return _losingPitcher;        } }
        public string SavingPitcher        { get { return _savingPitcher;        } }
        public string WinningBatter        { get { return _winningBatter;        } }
        public GameData(string[] gameData, Dictionary<string, TeamData> teamData)
        {
            _date = new DateTime(
                                 Convert.ToInt32(gameData[0].Substring(0,4)), // year
                                 Convert.ToInt32(gameData[0].Substring(4,2)), // month
                                 Convert.ToInt32(gameData[0].Substring(6,2))); // day
            _awayTeamAbbreviation = gameData[3];
            _homeTeamAbbreviation = gameData[6];
            _awayTeam = teamData[_awayTeamAbbreviation].City + " " + teamData[_awayTeamAbbreviation].NickName;
            _homeTeam = teamData[_homeTeamAbbreviation].City + " " + teamData[_homeTeamAbbreviation].NickName;
            _awayTeamScore = gameData[9];
            _homeTeamScore = gameData[10];
            _winningPitcher = gameData[94];
            _losingPitcher = gameData[96];
            _savingPitcher = (gameData[98] != "" ? gameData[98] : "(none)");
            _winningBatter = (gameData[100] != "" ? gameData[100] : "(none)");
        }
    }
}
