﻿/* UserInterface.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ksu.Cis300.BaseballScores
{
    /// <summary>
    /// User interface for storing information 
    /// about baseball games and retreving it
    /// </summary>
    public partial class UserInterface : Form
    {
        private Dictionary<string, TeamData> _teamsDefined = new Dictionary<string, TeamData>();
        private HashSet<string> _teamsInComboBox = new HashSet<string>();
        private Dictionary<TeamAndDate, List<GameData>> _gamesByTeamDate = new Dictionary<TeamAndDate, List<GameData>>();

        public UserInterface()
        {
            InitializeComponent();
            using (StreamReader sr = new StreamReader("TEAMABR.csv"))
            {
                while (!sr.EndOfStream)
                {
                    TeamData td = new TeamData(SplitRawTeamDataString(sr.ReadLine()));
                    _teamsDefined.Add(td.Abbreviation, td);
                    
                }
            }
        }
        private string[] SplitRawTeamDataString(string line)
        {
            return line.Split(',');
        }
        private string[] SplitRawGameDataString(string line)
        {
            const int NUMFIELDS = 101;
            string[] arr = new string[NUMFIELDS];
            for (int i = 0; i < NUMFIELDS; i++)
            {
                if (line[0] == '"')
                {
                    line = line.Substring(1);
                    arr[i] = line.Substring(0, line.IndexOf('"'));
                    line = line.Substring(line.IndexOf('"') + 2);
                }
                else
                {
                    arr[i] = line.Substring(0, line.IndexOf(','));
                    line = line.Substring(line.IndexOf(',') + 1);
                }
            }
            return arr;
        }
        private void uxButtonAddDataFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(openFileDialog.FileName))
                {
                    try
                    {
                        while (!sr.EndOfStream)
                        {
                            GameData gd = new GameData(SplitRawGameDataString(sr.ReadLine()), _teamsDefined);
                            TeamAndDate tdHome = new TeamAndDate(gd.HomeTeamAbbreviation, gd.Date);
                            TeamAndDate tdAway = new TeamAndDate(gd.AwayTeamAbbreviation, gd.Date);
                            if (!_gamesByTeamDate.ContainsKey(tdHome))
                                _gamesByTeamDate.Add(tdHome, new List<GameData>());
                            _gamesByTeamDate[tdHome].Add(gd);
                            if (!_gamesByTeamDate.ContainsKey(tdAway))
                                _gamesByTeamDate.Add(tdAway, new List<GameData>());
                            _gamesByTeamDate[tdAway].Add(gd);
                            if (!_teamsInComboBox.Contains(gd.HomeTeamAbbreviation))
                            {
                                uxComboBox.Items.Add(_teamsDefined[gd.HomeTeamAbbreviation]);
                                _teamsInComboBox.Add(gd.HomeTeamAbbreviation);
                            }
                            if (!_teamsInComboBox.Contains(gd.AwayTeamAbbreviation))
                            {
                                uxComboBox.Items.Add(_teamsDefined[gd.AwayTeamAbbreviation]);
                                _teamsInComboBox.Add(gd.AwayTeamAbbreviation);
                            }
                        }
                        uxComboBox.Enabled = true;
                        uxButtonGetGameResults.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }

            }
        }

        private void uxButtonGetGameResults_Click(object sender, EventArgs e)
        {
            TeamData selectedTeam = (TeamData)uxComboBox.SelectedItem;
            if (selectedTeam == null) return;
            TeamAndDate td = new TeamAndDate(selectedTeam.Abbreviation, uxDateTimePicker.Value);
            if (_gamesByTeamDate.ContainsKey(td))
            {
                foreach (GameData gd in _gamesByTeamDate[td])
                {
                    GameInformation gi = new GameInformation();
                    gi.Data = gd;
                    gi.Show();
                }
            }
            else
            {
                MessageBox.Show("No results for that team on that date.");
            }
        }
    }
}
